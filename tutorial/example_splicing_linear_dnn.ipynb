{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linear EIG path for splicing prediction: example with feed forward network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will generate attributions using linear IG (O-L-IG) and baselines:\n",
    "1. k-means\n",
    "2. median\n",
    "3. close\n",
    "4. random\n",
    "5. encoded-zero\n",
    "We will compute some baselines (k-means, median, close and random) in original feature space and encode-zero baseline in the latent space. Latent space refers to the encoded representation that is input to the decoder of an autoencoder. We assume models are run using tensorflow library. \n",
    "\n",
    "First, we will load all the relevant packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "import argparse\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "%matplotlib inline\n",
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "tf.compat.v1.disable_eager_execution()\n",
    "from tensorflow.python.saved_model import tag_constants\n",
    "from splicing_data_preprocessing import load_data_alt_const, get_tissue_specific_events, get_meta_feature_groups\n",
    "from splicing_data_preprocessing import get_constitutive_events, get_inc_events, get_samples\n",
    "from eig.interpreters.DeepNetworkModel import DeepNetworkModel\n",
    "from eig.interpreters.LatentModel import LatentModel\n",
    "from eig.interpreters.LinearInterpreter import LinearInterpreter\n",
    "from eig.interpreters.LinearInterpreterLatentBL import LinearInterpreterLatentBL\n",
    "from eig.feature_significance.FeatureSignificance import FeatureSignificance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we will declare all the constants used in this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Keys for the three prediction targets \n",
    "TARGET_PSI1 = \"psi_1\"\n",
    "TARGET_DPSI_UP = \"delta_psi_up\"\n",
    "TARGET_DPSI_DOWN = \"delta_psi_down\"\n",
    "# Target tensor mapping prediction targets with their tensors. \n",
    "TARGET_TENSOR = {TARGET_PSI1: \"output/psi_logits/BiasAdd:0\",\n",
    "                 TARGET_DPSI_UP: \"output/dpsi_logits_up/BiasAdd:0\",\n",
    "                 TARGET_DPSI_DOWN: \"output/dpsi_logits_down/BiasAdd:0\"}\n",
    "\n",
    "# Number of points for EIG linear paths\n",
    "NO_OF_POINTS = 250\n",
    "# Number of baseline points\n",
    "NO_BASELINES = 3\n",
    "# For efficieny reasons, limit the number of maximum points for k-means\n",
    "MAX_POINTS_FOR_K_MEANS = 20000\n",
    "\n",
    "#Baseline keys\n",
    "BASELINES_ZERO = \"zero\"\n",
    "BASELINES_ENCODER_ZERO = \"encoded_zero\"\n",
    "BASELINES_K_MEANS = \"k-means\"\n",
    "BASELINES_MEDIAN = \"median\"\n",
    "BASELINES_RANDOM = \"random\"\n",
    "\n",
    "# Sample type (inclusion, exclusion, no change)\n",
    "SAMPLE_TYPE_INC = \"_inc_idx\"\n",
    "SAMPLE_TYPE_EXC = \"_exc_idx\"\n",
    "SAMPLE_TYPE_NC = \"nc_change\"\n",
    "\n",
    "#Tissue pairs for deltaPSI\n",
    "TISSUE_PAIRS = ['Heart_Spleen', 'Heart_Hippocampus', 'Hippocampus_Liver', 'Hippocampus_Spleen', 'Hippocampus_Lung',\n",
    "                'Heart_Thymus', 'Lung_Spleen', 'Heart_Liver', 'Spleen_Thymus', 'Liver_Spleen',\n",
    "                'Liver_Lung', 'Liver_Thymus', 'Hippocampus_Thymus', 'Heart_Lung', 'Lung_Thymus']\n",
    "TISSUES = [\"Heart\", \"Hippocampus\", \"Liver\", \"Lung\", \"Spleen\", \"Thymus\"]\n",
    "\n",
    "# Put sample and target types \n",
    "sample_types = [\"_inc_idx\", \"_exc_idx\", \"nc_change\"]\n",
    "target_types = [\"delta_psi_down\", \"delta_psi_up\", \"psi_1\"]\n",
    "\n",
    "#Compute attributions for tissue of interest samples\n",
    "tissue_of_interest = \"Hippocampus\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will specify all the paths needed to run the EIG on splicing feed forward network. Make sure to download splicing data from [here](https://majiq.biociphers.org/interpretation-jha-et-al-2019/all_data.npz) and copy the all_data.npz file to the data folder before running the next step. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "encoder_model = \"../models/splicing/dnn_encoder/\"\n",
    "decoder_model = \"../models/splicing/dnn_decoder/\"\n",
    "splicing_model = \"../models/splicing/dnn_model/\"\n",
    "meta_features_file = \"../data/metaFeatures.txt\"\n",
    "input_data_file = \"../data/all_data.npz\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read the splicing data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get alternative splicing_old data dictionary and alternative and constitutive data dictionary\n",
    "alt_data_dict, alt_features, const_data_dict = load_data_alt_const(input_data_file)\n",
    "baseline_event_ids, baseline_data = get_constitutive_events(const_data_dict)\n",
    "    \n",
    "tissue_data_dict = get_tissue_specific_events(alt_data_dict,\n",
    "                                              tissue_of_interest)\n",
    "\n",
    "#Get feature names and feature->meta-feature mappings\n",
    "feature_names = tissue_data_dict[tissue_of_interest + '_feature_names']\n",
    "meta_feat_names, attr_groups = get_meta_feature_groups(feature_names, meta_features_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we load the splicing feed forward model, we assume that the model has been developed using tensorflow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "WARNING:tensorflow:From <ipython-input-5-8c739ed9e285>:7: load (from tensorflow.python.saved_model.loader_impl) is deprecated and will be removed in a future version.\n",
      "Instructions for updating:\n",
      "This function will only be available through the v1 compatibility library as tf.compat.v1.saved_model.loader.load or tf.compat.v1.saved_model.load. There will be a new function for importing SavedModels in Tensorflow 2.0.\n",
      "INFO:tensorflow:Restoring parameters from models/splicing/dnn_model/variables/variables\n"
     ]
    }
   ],
   "source": [
    "# Load the splicng model\n",
    "deep_graph = tf.Graph()\n",
    "deep_sess = tf.compat.v1.Session(graph=deep_graph)\n",
    "tf.compat.v1.saved_model.loader.load(\n",
    "            deep_sess,\n",
    "            [tag_constants.SERVING],\n",
    "            splicing_model\n",
    "        )\n",
    "\n",
    "#Specify the three prediction targets (inclusion, exclusion and no change)\n",
    "target_tensor_inc = deep_graph.get_tensor_by_name(TARGET_TENSOR[target_types[0]])\n",
    "target_tensor_exc = deep_graph.get_tensor_by_name(TARGET_TENSOR[target_types[1]])\n",
    "target_tensor_nc = deep_graph.get_tensor_by_name(TARGET_TENSOR[target_types[2]])\n",
    "input_1 = deep_graph.get_tensor_by_name(\"input:0\")\n",
    "tissue_id1 = deep_graph.get_tensor_by_name(\"layer_1/tissue_id_1:0\")\n",
    "tissue_id2 = deep_graph.get_tensor_by_name(\"layer_1/tissue_id_2:0\")\n",
    "\n",
    "#Initialize the deep neural network object for each of the three prediction targets\n",
    "deep_model_inc = DeepNetworkModel(deep_sess, ([input_1], [tissue_id1, tissue_id2], target_tensor_inc))\n",
    "deep_model_exc = DeepNetworkModel(deep_sess, ([input_1], [tissue_id1, tissue_id2], target_tensor_exc))\n",
    "deep_model_nc = DeepNetworkModel(deep_sess, ([input_1], [tissue_id1, tissue_id2], target_tensor_nc))\n",
    "\n",
    "deep_model_all = [deep_model_inc, deep_model_exc, deep_model_nc]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to use baselines or paths in the latent space, specify the encoder and decoder models. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Restoring parameters from models/splicing/dnn_encoder/variables/variables\n",
      "INFO:tensorflow:Restoring parameters from models/splicing/dnn_decoder/variables/variables\n"
     ]
    }
   ],
   "source": [
    "# Load the encoder model\n",
    "e_graph = tf.Graph()\n",
    "e_sess = tf.compat.v1.Session(graph=e_graph)\n",
    "tf.compat.v1.saved_model.loader.load(\n",
    "    e_sess,\n",
    "    [tag_constants.SERVING],\n",
    "    encoder_model\n",
    ")\n",
    "\n",
    "# get the encoder input and output\n",
    "features_placeholder = e_graph.get_tensor_by_name('input:0')\n",
    "latent_dimensions = e_graph.get_tensor_by_name('latent/z/Tanh:0')\n",
    "reconstructed_output = e_graph.get_tensor_by_name('decoder/x_reconstructed/BiasAdd:0')\n",
    "\n",
    "# Load the decoder model\n",
    "d_graph = tf.Graph()\n",
    "d_sess = tf.compat.v1.Session(graph=d_graph)\n",
    "tf.compat.v1.saved_model.loader.load(\n",
    "    d_sess,\n",
    "    [tag_constants.SERVING],\n",
    "    decoder_model\n",
    ")\n",
    "\n",
    "# Get the decoder input and output\n",
    "decoder_placeholder = d_graph.get_tensor_by_name('decoder_input:0')\n",
    "reconstructed_output = d_graph.get_tensor_by_name('sigmoid_output:0')\n",
    "\n",
    "# Initialize the autoencoder object with the encoder and decoder input and output.\n",
    "ae_model = LatentModel(e_sess, d_sess,\n",
    "                       ([features_placeholder],\n",
    "                        latent_dimensions, [decoder_placeholder], reconstructed_output))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Group specific Baselines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run attributions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will run linear IG with group specific baselines (k-means, median, close, random)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sample type :  _inc_idx\n",
      "Computed attributions for tissue Hippocampus and target _inc_idx\n",
      "sample type :  _exc_idx\n",
      "Computed attributions for tissue Hippocampus and target _exc_idx\n",
      "sample type :  nc_change\n",
      "Computed attributions for tissue Hippocampus and target nc_change\n"
     ]
    }
   ],
   "source": [
    "baseline_type = \"median\" #can be replaced with: \"k-means\", \"close\", \"random\"\n",
    "# Run a subset of splicing examples, for better results run all samples. \n",
    "num_samples = 600\n",
    "all_attrs = {}\n",
    "# Run attributions for the three prediction targets (inclusion, exclusion and no change)\n",
    "for sample_type, deep_model in zip(sample_types, deep_model_all):\n",
    "    samples_inc, tissue_1, tissue_2, sample_event_ids = get_samples(tissue_data_dict,\n",
    "                                                                    tissue_of_interest,\n",
    "                                                                    sample_type)\n",
    "\n",
    "    print(\"sample type : \", sample_type)\n",
    "    if len(baseline_data) > MAX_POINTS_FOR_K_MEANS:\n",
    "        if baseline_type == \"k-means\" or baseline_type == \"close\":\n",
    "            idx = np.random.choice(np.arange(0, len(baseline_data)), size=MAX_POINTS_FOR_K_MEANS)\n",
    "            baseline_data_sub = np.array(baseline_data[idx], copy=True)\n",
    "        else:\n",
    "            baseline_data_sub = np.array(baseline_data, copy=True)\n",
    "        this_key = \"baseline_const_\" + str(tissue_of_interest) + \"_\" + str(sample_type)\n",
    "        samples_local = samples_inc[0:num_samples]\n",
    "        baseline_tissue1 = np.repeat(tissue_1[0:num_samples],NO_OF_POINTS * NO_BASELINES, axis=0)\n",
    "        baseline_tissue2 = np.repeat(tissue_2[0:num_samples],NO_OF_POINTS * NO_BASELINES, axis=0)\n",
    "\n",
    "        li = LinearInterpreter([baseline_data_sub],\n",
    "                               [samples_local],\n",
    "                               other_data=[baseline_tissue1, baseline_tissue2],\n",
    "                               nn_model=deep_model,\n",
    "                               baseline_type=baseline_type,\n",
    "                               no_baselines=NO_BASELINES,\n",
    "                               no_points=NO_OF_POINTS)\n",
    "        attributes_li = li.attributions_subgroups(attr_groups)\n",
    "        all_attrs[this_key] = attributes_li[0]\n",
    "        print(\"Computed attributions for tissue {} and target {}\".format(tissue_of_interest, sample_type))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Feature significance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have attributions, we can find significant features using compute_feature_significance function from the FeatureSignificance class. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(600, 782) (1800, 782) (782,)\n",
      "number of significant features:  713\n"
     ]
    }
   ],
   "source": [
    "# Target class for feature significance\n",
    "target_key = \"baseline_const_\" + str(tissue_of_interest) + \"_\" + str(\"_inc_idx\")\n",
    "target_attributes = [np.abs(all_attrs[target_key])]\n",
    "\n",
    "# Background set for feature significance\n",
    "null_attributes = []\n",
    "null_keys = [\"baseline_const_\" + str(tissue_of_interest) + \"_\" + str(\"_exc_idx\"), target_key, \n",
    "              \"baseline_const_\" + str(tissue_of_interest) + \"_\" + str(\"nc_change\")]\n",
    "for ii in null_keys:\n",
    "    null_attributes.append(np.abs(all_attrs[ii]))\n",
    "\n",
    "# Get feature significance object\n",
    "fs = FeatureSignificance()\n",
    "\n",
    "# Run feature significance function \n",
    "significant_features = fs.compute_feature_significance(target_attributes,\n",
    "                                                       null_attributes,\n",
    "                                                       meta_feat_names,\n",
    "                                                       correction_alpha=0.05,\n",
    "                                                       correction_method='fdr_bh')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Group agnostic baselines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run attributions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will run linear IG in latent space (H-L-IG) with group agnostic baseline (encoded_zero) in the latent feature space.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sample type :  _inc_idx\n",
      "Computed attributions for tissue Hippocampus and target _inc_idx\n",
      "sample type :  _exc_idx\n",
      "Computed attributions for tissue Hippocampus and target _exc_idx\n",
      "sample type :  nc_change\n",
      "Computed attributions for tissue Hippocampus and target nc_change\n"
     ]
    }
   ],
   "source": [
    "baseline_type = \"encoded_zero\" \n",
    "NO_BASELINES = 1\n",
    "all_attrs_latent_bl = {}\n",
    "# Run attributions for the three prediction targets (inclusion, exclusion and no change)\n",
    "for sample_type, deep_model in zip(sample_types, deep_model_all):\n",
    "    samples_inc, tissue_1, tissue_2, sample_event_ids = get_samples(tissue_data_dict,\n",
    "                                                                    tissue_of_interest,\n",
    "                                                                    sample_type)\n",
    "\n",
    "    print(\"sample type : \", sample_type)\n",
    "    # For running times, we limit the number of data points for k-means.\n",
    "    if len(baseline_data) > MAX_POINTS_FOR_K_MEANS:\n",
    "        if baseline_type == \"k-means\" or baseline_type == \"close\":\n",
    "            idx = np.random.choice(np.arange(0, len(baseline_data)), size=MAX_POINTS_FOR_K_MEANS)\n",
    "            baseline_data_sub = np.array(baseline_data[idx], copy=True)\n",
    "        else:\n",
    "            baseline_data_sub = np.array(baseline_data, copy=True)\n",
    "        this_key = \"baseline_encoded_zero_\" + str(tissue_of_interest) + \"_\" + str(sample_type)\n",
    "        samples_local = samples_inc\n",
    "        baseline_tissue1 = np.repeat(tissue_1,NO_OF_POINTS * NO_BASELINES, axis=0)\n",
    "        baseline_tissue2 = np.repeat(tissue_2,NO_OF_POINTS * NO_BASELINES, axis=0)\n",
    "\n",
    "        li_lbl = LinearInterpreterLatentBL([baseline_data_sub, None, None],\n",
    "                                           [samples_local, None, None],\n",
    "                                           other_data=[baseline_tissue1, baseline_tissue2],\n",
    "                                           autoencoder=ae_model,\n",
    "                                           nn_model=deep_model,\n",
    "                                           baseline_type=baseline_type,\n",
    "                                           no_baselines=NO_BASELINES,\n",
    "                                           no_points=NO_OF_POINTS)\n",
    "        attributes_li = li_lbl.attributions_subgroups(attr_groups)\n",
    "        all_attrs_latent_bl[this_key] = attributes_li[0]\n",
    "        print(\"Computed attributions for tissue {} and target {}\".format(tissue_of_interest, sample_type))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Feature Significance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have attributions, we can find significant features using compute_feature_significance function from the FeatureSignificance class. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1655, 782) (3446, 782) (782,)\n",
      "number of significant features:  668\n"
     ]
    }
   ],
   "source": [
    "# Target class for feature significance\n",
    "target_key = \"baseline_encoded_zero_\" + str(tissue_of_interest) + \"_\" + str(\"_inc_idx\")\n",
    "target_attributes = [np.abs(all_attrs_latent_bl[target_key])]\n",
    "\n",
    "# Background set for feature significance\n",
    "null_attributes = []\n",
    "null_keys = [target_key, \"baseline_encoded_zero_\" + str(tissue_of_interest) + \"_\" + str(\"_exc_idx\"),\n",
    "              \"baseline_encoded_zero_\" + str(tissue_of_interest) + \"_\" + str(\"nc_change\")]\n",
    "for ii in null_keys:\n",
    "    null_attributes.append(np.abs(all_attrs_latent_bl[ii]))\n",
    "\n",
    "# Get feature significance object\n",
    "fs = FeatureSignificance()\n",
    "\n",
    "# Run feature significance function \n",
    "significant_features = fs.compute_feature_significance(target_attributes,\n",
    "                                                       null_attributes,\n",
    "                                                       meta_feat_names,\n",
    "                                                       correction_alpha=0.05,\n",
    "                                                       correction_method='fdr_bh')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
