Module Overview
=======================================


.. autoclass:: eig.DeepNetworkModel
   :members:

|
.. autoclass:: eig.LatentModel
   :members:

|
.. autoclass:: eig.LinearInterpreter
   :members:

|
.. autoclass:: eig.LatentLinearInterpreter
   :members:

|
.. autoclass:: eig.NeighborsInterpreter
   :members:

|
.. autoclass:: eig.LatentNeighborsInterpreter
   :members:

|
.. autoclass:: eig.FeatureSignificance
   :members: